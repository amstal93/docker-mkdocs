#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Build the HTML MkDocs documentation.
#
# Examples:
#   ./build-html.sh
#   ./build-html.sh --name my-documentation
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

"${PROJECT_DIR}/bin/build.sh" "${@}"

exit 0
